#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import socket
import subprocess
import unittest

SERVER_IP = '127.0.0.1'
SERVER_PORT = '6003'

# Maximum time we allow the client to run in its own thread (sec.)
MAX_TIME_SERVER = 3
# Time to wait for server to receive messages (ack, usually)
WAIT_SERVER = 1

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')


class TestCommandLine(unittest.TestCase):
    usage = "Usage: python3 server.py <IP> <port> <audio_file>\n"

    def test_noargs(self):
        output = subprocess.run(['python3', 'server.py'],
                                timeout=MAX_TIME_SERVER,
                                cwd=parent_dir, text=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        self.assertEqual(self.usage, output.stdout)

    def test_toomuch_args(self):
        output = subprocess.run(['python3', 'server.py', SERVER_IP,
                                 SERVER_PORT, 'file', 'extra'],
                                timeout=MAX_TIME_SERVER,
                                cwd=parent_dir, text=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        self.assertEqual(self.usage, output.stdout)


class TestWithClientFirst(unittest.TestCase):

    def setUp(self):
        self.server = SERVER_IP
        self.port = SERVER_PORT
        self.process = subprocess.Popen(['python3', 'server.py',
                                         self.server, self.port, 'file'],
                                        cwd=parent_dir, text=True,
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.STDOUT)
        try:
            # Check if server dies before WAIT_SERVER
            self.process.communicate(timeout=WAIT_SERVER)
            self.fail("Server died")
        except subprocess.TimeoutExpired:
            # Server booted up and didn't die
            pass

    def tearDown(self):
        self.process.kill()
        self.process.communicate()

    def test_invite(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.settimeout(MAX_TIME_SERVER)
            request = f"INVITE sip:user@{self.server} SIP/2.0"
            my_socket.sendto(request.encode('utf-8'), (self.server, int(self.port)))
            response = my_socket.recv(2048).decode('utf-8')
            ack = f"ACK sip:user@{self.server} SIP/2.0"
            my_socket.sendto(ack.encode('utf-8'), (self.server, int(self.port)))
        self.assertEqual("SIP/2.0 200 OK", response.splitlines()[0])

    def test_bad_request(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.settimeout(MAX_TIME_SERVER)
            request = f"INVITE sip:user@{self.server}"
            my_socket.sendto(request.encode('utf-8'), (self.server, int(self.port)))
            response = my_socket.recv(2048).decode('utf-8')
            print("Response", response)
        self.assertEqual("SIP/2.0 400 Bad Request", response.splitlines()[0])

    def test_bad_method(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.settimeout(MAX_TIME_SERVER)
            request = f"INVIT sip:user@{self.server} SIP/2.0"
            my_socket.sendto(request.encode('utf-8'), (self.server, int(self.port)))
            response = my_socket.recv(2048).decode('utf-8')
        self.assertEqual("SIP/2.0 405 Method Not Allowed", response.splitlines()[0])

    def test_bye(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.settimeout(MAX_TIME_SERVER)
            request = f"BYE sip:user@{self.server} SIP/2.0"
            my_socket.sendto(request.encode('utf-8'), (self.server, int(self.port)))
            response = my_socket.recv(2048).decode('utf-8')
        self.assertEqual("SIP/2.0 200 OK", response.splitlines()[0])
